import { extendTheme } from "@chakra-ui/react";

const colors = {
  primary: {
    100: "#E5FCF1",
    200: "#27EF96",
    300: "#10DE82",
    400: "#0EBE6F",
    500: "#0CA25F",
    600: "#0A864F",
    700: "#086F42",
    800: "#075C37",
    900: "#064C2E"
  },
  secondary: {
    100: "#319795",
    200: "#2C8786",
    300: "#277877",
    400: "#226968",
    500: "#1D5A59",
    600: "#184B4A",
    700: "#133C3B",
    800: "#0E2D2C",
    900: "#091E1D"
  }
};

const customTheme = extendTheme({ colors });

export default customTheme;