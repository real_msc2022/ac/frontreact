import React from "react";
import { Box,Image,Text, Flex } from "@chakra-ui/react"

export default function Logo(props) {
  return (
    <Box display="flex" alignItems="center" gridGap={2} {...props}>
      <Image src="logo.png" alt="Autocoin" />
      <Text color={"secondary.600"} fontWeight="bold">
        Auto<br/>Coin
      </Text>
    </Box>
  );
}