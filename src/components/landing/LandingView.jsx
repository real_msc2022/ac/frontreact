  
import React from "react";
import { Flex } from "@chakra-ui/react";
import Header from "../../ui/header/Header";
import Hero from "../../ui/hero/Hero";
import FeaturesSplit from "../features/FeaturesSplitView";

export default function LandingView(props) {
  return (
    <Flex
      direction="column"
      align="center"
      maxW={{ xl: "1200px" }}
      m="0 auto"
      {...props}
    >
      <Header />
      <Hero/>
      <FeaturesSplit/>
      {props.children}
    </Flex>
  );
}