import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "./components/landing";
import {
  ChakraProvider
} from '@chakra-ui/react';
import customTheme from './utils/theme';
function App() {
  return (
    
    <ChakraProvider theme={customTheme}>
             <Router>
      <Switch>
        <Route path="/landing">
          <Landing />
        </Route>
      </Switch>
    </Router>
    </ChakraProvider>
  );
}

export default App;
