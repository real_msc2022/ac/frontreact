# Autocoin Frontend
## Setup containerized "micro"-service
```s
docker -v
docker-compose -v
docker-compose up -d --build
curl localhost:3000
```

## Docker details
This project can be run with Docker containers. To simplify Build and Run, parameters are consigned into Docker-Compose orchestrator.
### Docker Troubleshooting
#### Docker setup on ubuntu 20.04
```css
sudo apt-get install docker docker-compose -y
sudo usermod -aG docker $USER
```
and relog session.
#### Docker can't find Daemon
check `docker info` and `sudo docker info`

## Structure
```css
React
├── docker-compose.yml
├── Dockerfile
├── .dockerignore
├── .env.sample
├── .eslintcache
├── .eslintignore
├── .eslintrcTEST
├── .git
├── .gitignore
├── .gitlab-ci.yml
├── node_modules
├── package.json
├── package-lock.json
├── .prettierrc
├── public
├── README.md
└── src
    ├── App.js
    ├── App.test.js
    ├── ColorModeSwitcher.js
    ├── components
    │   ├── features
    │   │   └── FeaturesSplitView.jsx
    │   └── landing
    │       ├── index.js
    │       ├── LandingView.jsx
    │       └── LandingView.style.js
    ├── index.js
    ├── Logo.js
    ├── logo.svg
    ├── reportWebVitals.js
    ├── serviceWorker.js
    ├── setupTests.js
    ├── test-utils.js
    ├── ui
    │   ├── header
    │   │   └── Header.jsx
    │   ├── hero
    │   │   └── Hero.jsx
    │   └── logo
    │       └── Logo.jsx
    └── utils
        └── theme.js

```
built with `tree -qa -L 1`


## NODE JS section
---
### React js based project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `npm run eject`

NB: eject has been executed in order to fire docker - did not compile otherwise.

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

#### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

#### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

#### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

#### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

#### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

#### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify


**Obsolete section need refactor**
## Lifecycle
TEMPLATE is designed to make learning curve as short as possible while providing flexibility. We tried to provide clean and understandable code by separating data from views and logic.

This part of the documentation will answer your questions about basic lifecycle of the project and provide some insights about files.
```s

src/index.js
Default entry point of the application. It loads theme style and includes AppRenderer to the project.

Styling for plugins which will be used throughout the project are included here. For example, Bootstrap style file included and overridden by theme styling file.

Default color theme value is retrieved and style file loaded at this file. The value of the default theme is located at src/constants/dafaultValues.js file. Other theme related settings are also located at the defaultValues file.


src/AppRenderer.js
This is the file which loads src/App.js after theme styling is done. It is the second file on lifecycle and act as a Middleware to initialize React Dom rendering.


src/App.js
First job of this file is to load plugins and libraries which does not have any connection with a layout. These plugins an libraries are Firebase, ColorSwitcher and NotificationContainer.

This file also acts as a first route of the project. It makes authorization control via ProtectedRoute function and also makes necessary route direction to sub routes.


src/views/index.js
If the project should have a home or a landing page, this is the place where it should be rendered. We have created the file to act as a guidance but since the project does not contain a home file, it only redirects to 'app' route.


src/views/error.js
A page to show customized page for errors. The routes that can not be found redirected to this page.


src/views/app/index.js
Application related pages(except authorization) are decorated with AppLayout at this page. It also creates routing with a switch case to its sub directories.


src/views/app/applications
src/views/app/dashboards
src/views/app/menu
src/views/app/pages
src/views/app/ui
All the index.js files under these directories manages routing separately. This routing process allows easily creating sub layouts if it is necessary. Other files than index.js are sub pages of the project.


src/views/user/index.js
Authorization related pages are decorated with UserLayout at this file. It is the second layout of the project and does a similar job with AppLayout. If you require another distinct layout for your project you may follow this approach. All routing to sub files under this directory is done by the file with a switch.
```


### TEST CI CODE

https://gitlab.com/real_msc2022/ac/frontreact/-/ci/lint
